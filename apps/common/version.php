<?php
return array(
    // 应用版本
    'app_version' => '3.2.4',
    
    // 发布时间
    'release_time' => '20230228',

    // 修订版本
    'revise_version' => '5'

);
